﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using DBcon;


namespace SafeSynTools
{
    public class SqlDbHelper
    {
        //连接热数据库(azure)
        public static string azureconnstr = ConfigurationManager.ConnectionStrings["sqlConnectionString_azure"].ToString();
        //连接冷数据库(本地)
        public static string localconnnStr = ConfigurationManager.ConnectionStrings["sqlConnectionString_local"].ToString();
        public SqlDbHelper()
        {
        }
        /// <summary>
        /// 从azure数据库中拿数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromAzure(string sql)
        {
         
            SqlConnection azureconn = new SqlConnection(azureconnstr);
            DataSet ds1 = new DataSet();
            try
            {
                azureconn.Open();                
                SqlDataAdapter sda = new SqlDataAdapter(sql, azureconn);
                sda.Fill(ds1);
            }
            catch(Exception ex)
            {
                
            }
            finally
            {
                if (azureconn.State == ConnectionState.Open)
                {
                    azureconn.Dispose();
                    azureconn.Close();
                }
            }
            return ds1.Tables[0];
        }
       /// <summary>
       /// 用来判断数据是否存在
       /// </summary>
       /// <param name="strsql"></param>
       /// <returns></returns>
       public static DataTable GetQueryFromLocalData(string strsql)
       {
           SqlConnection localconn = new SqlConnection(localconnnStr);
           DataSet ds1 = new DataSet();
           try
           {
               localconn.Open();
               SqlDataAdapter sda = new SqlDataAdapter(strsql, localconn);
               sda.Fill(ds1);
           }
           catch 
           {
              
           }
           finally
           {
               if (localconn.State == ConnectionState.Open)
               {
                   localconn.Dispose();
                   localconn.Close();
               }
           }
           return ds1.Tables[0];
       }
        /// <summary>
        /// 用来判断数据是否存在
        /// </summary>
        /// <param name="strsql"></param>
        /// <returns></returns>
       public static int GetQueryFromLocal(string strsql)
       {
            SqlConnection localconn = new SqlConnection(localconnnStr);
            DataSet ds1 = new DataSet();
            try
            {
                localconn.Open();
                SqlDataAdapter sda = new SqlDataAdapter(strsql, localconn);
                sda.Fill(ds1);
            }
            catch 
            {
            }
            finally
            {
                if (localconn.State == ConnectionState.Open)
                {
                    localconn.Dispose();
                    localconn.Close();
                }
            }
            return ds1.Tables[0].Rows.Count;
       }
       /// <summary>
       /// 执行存储过程
       /// </summary>
       /// <param name="dt"></param>
       /// <param name="maxlockid"></param>
       /// <returns></returns>
       public static int RunProCess(string proName)
       {
           SqlConnection conn = new SqlConnection(localconnnStr);
           int resulte = 1;
           try
           {
               //dt.Columns.Remove("lockid");
               conn.Open();
               SqlCommand cmd = new SqlCommand();
               cmd.Connection = conn;
               cmd.CommandText = proName;
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.ExecuteNonQuery();
           }
           catch (Exception ex)
           {
               resulte = 0;
           }
           finally
           {
               if (conn.State == ConnectionState.Open)
               {
                   conn.Dispose();
                   conn.Close();
               }
           }
           return resulte;
       }
      public static int RunInsert(DataTable dt,string maxlockid,string ProName)
       {
           SqlConnection conn = new SqlConnection(localconnnStr);
           int resulte=0;
           try
           {
               //dt.Columns.Remove("lockid");
               conn.Open();
               SqlCommand cmd = new SqlCommand();
               cmd.Connection = conn;
               cmd.CommandText = ProName;
               cmd.CommandTimeout = 500000;
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add("@userInData", SqlDbType.Structured);
               cmd.Parameters.Add("@mxlockid", SqlDbType.VarChar);
               cmd.Parameters.Add("@Result", SqlDbType.Int);
               cmd.Parameters[0].Value = dt;
               cmd.Parameters[1].Value = maxlockid;
               cmd.Parameters["@Result"].Direction = ParameterDirection.ReturnValue;               
               cmd.ExecuteNonQuery();
               resulte=(int)cmd.Parameters["@Result"].Value;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           finally
           {
               if (conn.State == ConnectionState.Open)
               {
                   conn.Dispose();
                   conn.Close();
               }               
           }
          return  resulte;
       }
        /// <summary>
        /// 对azure数据修改或插入的方法
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static void ExecuteNonQueryToAzure(string sql)
       {
           SqlConnection azureconn = new SqlConnection(azureconnstr);
           try
           {
               azureconn.Open();
               SqlCommand loclsqlcmd = new SqlCommand(sql, azureconn);
               loclsqlcmd.ExecuteNonQuery();
           }
           catch 
           {
               
           }
           finally
           {
               if (azureconn.State == ConnectionState.Open)
               {
                   azureconn.Dispose();
                   azureconn.Close();
               }
           }
       }
         /// <summary>
       /// 对本地数据库修改或插入的方法
       /// </summary>
       /// <param name="sql"></param>
       /// <returns></returns>
       public static void ExecuteNonQueryToLocal(string sql)
       {
          //  localconnnStr = "Data Source=13.75.93.215;uid=report2;pwd=123456@qq.com;database=dafacloud;Connect Timeout=90000;";
            SqlConnection localconn = new SqlConnection(localconnnStr);
           try
           {
               localconn.Open();
               SqlCommand loclsqlcmd = new SqlCommand(sql, localconn);
               loclsqlcmd.ExecuteNonQuery();
           }
           catch  (Exception e)
           {
               
           }
           finally
           {
               if (localconn.State == ConnectionState.Open)
               {
                   localconn.Dispose();
                   localconn.Close();
               }
           }
       }
        /// <summary>
        /// 从azure数据库中拿数据
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromAzure(string sql, SqlParameter[] cmdParms)
        {
            using (SqlConnection azureconn = new SqlConnection(azureconnstr))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, azureconn, null, sql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {

                }
                finally
                {
                    if (azureconn.State == ConnectionState.Open)
                    {
                        azureconn.Dispose();
                        azureconn.Close();
                    }
                }
                if (ds1.Tables.Count > 0)
                {
                    return ds1.Tables[0];
                }
                else
                {
                    return null;
                }
            }

        }

        /// <summary>
        /// 用来判断数据是否存在
        /// </summary>
        /// <param name="strsql"></param>
        /// <returns></returns>
        public static DataTable GetQueryFromLocalData(string strsql, SqlParameter[] cmdParms)
        {
            using (SqlConnection localconn = new SqlConnection(localconnnStr))
            {
                DataSet ds1 = new DataSet();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    PrepareCommand(cmd, localconn, null, strsql, cmdParms);
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds1);
                        cmd.Parameters.Clear();
                    }

                }
                catch
                {

                }
                finally
                {
                    if (localconn.State == ConnectionState.Open)
                    {
                        localconn.Dispose();
                        localconn.Close();
                    }
                }
                if (ds1.Tables.Count > 0)
                {
                    return ds1.Tables[0];
                }
                else
                {
                    return null;
                }

            }

        }
        private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            if (trans != null)
                cmd.Transaction = trans;
            cmd.CommandType = CommandType.Text;//cmdType;
            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }
    }
}
