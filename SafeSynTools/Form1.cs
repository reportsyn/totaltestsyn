﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Transactions;

namespace SafeSynTools
{
    public partial class Form1 : Form
    {
        int seconds_time = Convert.ToInt32(ConfigurationManager.AppSettings["seconds_time"]);//获取实时同步的执行间隔时间
        string errorLogPath = System.Environment.CurrentDirectory + "\\errorlog.txt";//错误日志记录路径
        string successLogPath = System.Environment.CurrentDirectory + "\\successlog.txt";//成功日志记录路径
        System.Timers.Timer timer_clear = new System.Timers.Timer();

        public Form1()
        {
            InitializeComponent();
            seconds_time = seconds_time * 1000;//秒
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            //创建实时同步的线程
            Thread thread = new Thread(new ThreadStart(syn_sysSum_test));
            thread.Start();

            timer_clear.AutoReset = false;
            timer_clear.Interval = 10000;
            timer_clear.Elapsed += new System.Timers.ElapsedEventHandler(clear_Elapse);
            timer_clear.Start();
        }

        #region 实时同步的方法
        void syn_sysSum_test()
        {
            string tableString = ConfigurationManager.AppSettings["onlyOne_test"].ToString();
            if (string.IsNullOrEmpty(tableString) == false)
            {
                FillMsg1("正在同步...");
                string[] tablenames = tableString.Split(',');
                while (true)
                {
                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour >= 3 && DateTime.Now.Hour < 6)
                    {
                        Thread.Sleep(1000 * 60 * 60 * 3);
                        continue;
                    }
                    foreach (var tablename in tablenames)
                    {
                        try
                        {
                            TimeSpan ts1 = new TimeSpan(DateTime.Now.Ticks);
                            List<SqlParameter> LocalSqlParamter = new List<SqlParameter>(){new SqlParameter("@tableName",tablename)};
                            string querystr = string.Format("select lockid from dt_tablelockid_test where tableName=@tableName", tablename);
                            var locktable = SqlDbHelper.GetQueryFromLocalData(querystr, LocalSqlParamter.ToArray());
                            byte[] lockid = StringConvertByte(locktable.Rows[0]["lockid"].ToString());
                            List<SqlParameter> AzureSqlParamter = new List<SqlParameter>()
                            {
                                new SqlParameter("@lockid", SqlDbType.Timestamp){ Value=lockid},
                                new SqlParameter("@flog1", SqlDbType.Int){ Value=1},
                                new SqlParameter("@state1", SqlDbType.Int){ Value=1},
                                new SqlParameter("@state3", SqlDbType.Int){ Value=3},
                                new SqlParameter("@type3", SqlDbType.Int){ Value=3},
                                new SqlParameter("@type9", SqlDbType.Int){ Value=9},
                                new SqlParameter("@type10", SqlDbType.Int){ Value=10},
                                new SqlParameter("@openstate1", SqlDbType.Int){ Value=1},
                                new SqlParameter("@openstate2", SqlDbType.Int){ Value=2}
                            };
                            List<string> maxLockid_list = new List<string>();
                            string columns = "*";
                            if (tablename == "dt_lottery_winning_record")
                                columns = "id,identityid,user_id,lottery_code,bonus_money,add_time,SourceName,lockid";
                            else if (tablename == "dt_user_out_account")
                                columns = "*";
                            string str = "select top 1000 " + columns + " from " + tablename + " where lockid>@lockid and ";
                            if(tablename == "dt_user_in_account" || tablename == "dt_user_get_point_record")
                                str = str + "user_id in(select id from dt_users where flog=@flog1) order by lockid asc";
                            else if (tablename == "dt_user_out_account")
                                str = str + "user_id in(select id from dt_users where flog=@flog1) and ((type in(@type3, @type9, @type10) and state = @state1) or (type = @type3 and state = @state3)) order by lockid asc";
                            else if (tablename == "dt_lottery_orders")
                                str = str + "flog=@flog1 AND openstate in(@openstate1,@openstate2) order by lockid asc";
                            else if (tablename == "dt_lottery_winning_record")
                                str = str + "flog=@flog1 order by lockid asc";
                            var table = SqlDbHelper.GetQueryFromAzure(str, AzureSqlParamter.ToArray());

                            if (table.Rows.Count != 0)//有数据时更新到本地数据库中
                            {
                                for (int i = table.Rows.Count - 1; i >= 0; i--)
                                {
                                    byte[] a = (byte[])table.Rows[i]["lockid"];
                                    string lockid_list = BitConverter.ToString(a).Replace("-", "");
                                    maxLockid_list.Add(lockid_list);
                                }
                                string maxlockid = "0x" + maxLockid_list.Max();
                                table.Columns.Remove("lockid");
                                int result = 0;
                                if (tablename == "dt_user_in_account")
                                    result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_in_account_synSum_test");
                                else if (tablename == "dt_user_out_account")
                                    result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_out_account_synSum_test");
                                else if (tablename == "dt_lottery_orders")
                                    result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_orders_synSum_test");
                                else if (tablename == "dt_lottery_winning_record")
                                    result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_lottery_winning_synSum_test");
                                else if (tablename == "dt_user_get_point_record")
                                    result = SqlDbHelper.RunInsert(table, maxlockid, "dsp_user_get_point_synSum_test");

                                TimeSpan ts2 = new TimeSpan(DateTime.Now.Ticks);
                                TimeSpan ts = ts1.Subtract(ts2).Duration();
                                string dateDiff = ts.Seconds.ToString() + "秒";
                                if (tablename == "dt_user_in_account" || tablename == "dt_user_out_account")
                                    FillMsg1(tablename + "成功同步汇总" + table.Rows.Count + "条数据,耗时:" + dateDiff);
                                else if (tablename == "dt_lottery_orders")
                                    FillMsg2(tablename + "成功同步汇总" + table.Rows.Count + "条数据,耗时:" + dateDiff);
                                else
                                    FillMsg3(tablename + "成功同步汇总" + table.Rows.Count + "条数据,耗时:" + dateDiff);
                                WriteLogData(tablename + "成功同步汇总" + table.Rows.Count + "条数据,耗时:" + dateDiff + "    同步汇总时间:" + DateTime.Now.ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message != "未将对象引用设置到对象的实例。")
                            {
                                FillErrorMsg(tablename + ":" + ex);
                                WriteErrorLog(tablename + ":" + DateTime.Now.ToString(), ex.ToString());
                            }
                        }
                    }
                    Thread.Sleep(seconds_time);//睡眠时间
                }
            }
        }

        private byte[] StringConvertByte(string sqlstring)
        {
            string stringFromSQL = sqlstring;
            List<byte> byteList = new List<byte>();

            string hexPart = stringFromSQL.Substring(2);
            for (int i = 0; i < hexPart.Length / 2; i++)
            {
                string hexNumber = hexPart.Substring(i * 2, 2);
                byteList.Add((byte)Convert.ToInt32(hexNumber, 16));
            }

            byte[] original = byteList.ToArray();
            return original;
        }
        #endregion

        #region richTextBox记录
        private delegate void RichBox1(string msg);
        private void FillMsg1(string msg)
        {
            if (richTextBox1.InvokeRequired)
            {
                RichBox1 rb = new RichBox1(FillMsg1);
                richTextBox1.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.AppendText(msg);
                    richTextBox1.AppendText("\r\n");
                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.Focus();
                }
            }
        }

        private delegate void RichBox2(string msg);
        private void FillMsg2(string msg)
        {
            if (richTextBox2.InvokeRequired)
            {
                RichBox2 rb = new RichBox2(FillMsg2);
                richTextBox2.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox2.IsHandleCreated)
                {
                    richTextBox2.AppendText(msg);
                    richTextBox2.AppendText("\r\n");
                    richTextBox2.SelectionStart = richTextBox2.Text.Length;
                    richTextBox2.SelectionLength = 0;
                    richTextBox2.Focus();
                }
            }
        }

        private delegate void RichBox3(string msg);
        private void FillMsg3(string msg)
        {
            if (richTextBox3.InvokeRequired)
            {
                RichBox3 rb = new RichBox3(FillMsg3);
                richTextBox3.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (richTextBox3.IsHandleCreated)
                {
                    richTextBox3.AppendText(msg);
                    richTextBox3.AppendText("\r\n");
                    richTextBox3.SelectionStart = richTextBox3.Text.Length;
                    richTextBox3.SelectionLength = 0;
                    richTextBox3.Focus();
                }
            }
        }

        private delegate void RichBoxErr(string msg);
        private void FillErrorMsg(string msg)
        {
            if (errorBox.InvokeRequired)
            {
                RichBoxErr rb = new RichBoxErr(FillErrorMsg);
                errorBox.Invoke(rb, new object[] { msg });
            }
            else
            {
                if (errorBox.IsHandleCreated)
                {
                    errorBox.AppendText(msg);
                    errorBox.AppendText("\r\n");
                    errorBox.SelectionStart = errorBox.Text.Length;
                    errorBox.SelectionLength = 0;
                    errorBox.Focus();
                }
            }
        }
        #endregion

        #region 打印成功日志记录
        private object obj1 = new object();
        public void WriteLogData(string msgex)
        {
            lock (obj1)
            {
                if (!File.Exists(successLogPath))
                {
                    FileStream fs1 = new FileStream(successLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(msgex);
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(successLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(msgex);
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 错误日志记录
        private object obj = new object();
        public void WriteErrorLog(string msgex, string msgsql)
        {
            lock (obj)
            {
                if (!File.Exists(errorLogPath))
                {
                    FileStream fs1 = new FileStream(errorLogPath, FileMode.Create, FileAccess.Write);//创建写入文件 
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(msgex);
                    sw.WriteLine(msgsql);
                    sw.WriteLine();
                    sw.WriteLine();
                    sw.Close();
                    fs1.Close();
                }
                else
                {
                    FileStream fs = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write);
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(msgex);
                    sr.WriteLine(msgsql);
                    sr.WriteLine();
                    sr.WriteLine();
                    sr.Close();
                    fs.Close();
                }
            }
        }
        #endregion

        #region 清理textbox
        private void clear_Elapse(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (System.DateTime.Now.ToString("mm") == "40")
                    ClearMsg();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex2)
            {

            }
            finally
            {
                timer_clear.Start();
            }
        }
        private delegate void RichBoxClear();
        private void ClearMsg()
        {
            if (richTextBox1.InvokeRequired & richTextBox2.InvokeRequired & richTextBox3.InvokeRequired & errorBox.InvokeRequired)
            {
                RichBoxClear rb = new RichBoxClear(ClearMsg);
                richTextBox1.Invoke(rb);
                richTextBox2.Invoke(rb);
                richTextBox3.Invoke(rb);
                errorBox.Invoke(rb);
            }
            else
            {
                if (richTextBox1.IsHandleCreated)
                {
                    richTextBox1.Clear();
                    richTextBox2.Clear();
                    richTextBox3.Clear();
                    errorBox.Clear();
                }
            }
        }
        #endregion

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
            System.Environment.Exit(0);
        }

    }
}
